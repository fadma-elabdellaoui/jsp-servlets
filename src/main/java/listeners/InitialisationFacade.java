package listeners;

import facade.FacadeParis;
import facade.FacadeParisStaticImpl;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

@WebListener
public class InitialisationFacade implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        FacadeParis facadeParis = new FacadeParisStaticImpl();
        /* facade mnt est une variable d'application*/
        sce.getServletContext().setAttribute("facade",facadeParis);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
