package controleur;

import facade.FacadeParis;
import facade.exceptions.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import modele.Match;
import modele.Pari;
import modele.Utilisateur;

import java.io.IOException;
import java.time.LocalDateTime;

@WebServlet(name = "controleur", urlPatterns = "/home/*")

public class Controleur extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();
        String[] parties = uri.split("/");
        String cleNav = parties[parties.length-1];
        String dest = "/WEB-INF/pages/accueil.jsp";
        FacadeParis facade = (FacadeParis) this.getServletContext().getAttribute("facade");
        Utilisateur user = (Utilisateur) req.getSession().getAttribute("user");
        switch (cleNav){
            case "deconnexion":
                facade.deconnexion(user.getLogin());
                req.getSession().invalidate();
                dest = "/WEB-INF/pages/accueil.jsp";
                break;
            case "mesParis":
                req.getSession().setAttribute("mesparis",facade.getMesParis(user.getLogin()));
                dest = "/WEB-INF/pages/mesParis.jsp";
                break;
            case "listeParisOuverts" :
                req.getSession().setAttribute("matchsPasCommences",facade.getMatchsPasCommences());
                dest = "/WEB-INF/pages/listeParisOuverts.jsp";
                break;
            case "menu":
                dest = "/WEB-INF/pages/menu.jsp";
                break;
            case "parier":
                long idmatch = Long.parseLong(req.getParameter("id"));
                Match m = facade.getMatch(idmatch);
                req.setAttribute("match",m);
                dest = "/WEB-INF/pages/parieMatch.jsp";
                break;
            case "annulerPari":
                long idPari = Long.parseLong(req.getParameter("id"));
                Pari pari = facade.getPari(idPari);
                if (pari == null){
                    String erreur = "le pari demandé n'existe pas";
                    req.setAttribute("erreur",erreur);
                    dest = "/WEB-INF/pages/mesParis.jsp";
                    break;
                }
                try {
                    facade.annulerPari(user.getLogin(),idPari);
                    req.setAttribute("pari",pari);
                    dest = "/WEB-INF/pages/msgPariAnnulee.jsp";
                } catch (OperationNonAuthoriseeException e) {
                    String erreur = e.getMessage();
                    req.setAttribute("erreur",erreur);
                    dest = "/WEB-INF/pages/mesParis.jsp";
                }
                break;
            case "ajoutMatch":
                dest = "/WEB-INF/pages/ajoutMatch.jsp";
                break;
            case "matchs":
                req.getSession().setAttribute("matchs",facade.getAllMatchs());
                dest = "/WEB-INF/pages/matchs.jsp";
                break;
            case "ajoutResultatMatch":
                long idm = Long.parseLong(req.getParameter("id"));
                Match match = facade.getMatch(idm);
                req.setAttribute("match",match);
                dest = "/WEB-INF/pages/ajoutResultatMatch.jsp";
                break;
            default:
                dest = "/WEB-INF/pages/accueil.jsp";
        }
        this.getServletContext().getRequestDispatcher(dest).forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();
        String[] parties = uri.split("/");
        String cleNav = parties[parties.length-1];
        String dest = "/WEB-INF/pages/accueil.jsp";
        /*j'ai récupéré facade */
        FacadeParis facade = (FacadeParis) this.getServletContext().getAttribute("facade");

        switch (cleNav){
            case "connexion":
                String login = req.getParameter("login");
                String  password = req.getParameter("password");
                try {
                    Utilisateur user = facade.connexion(login,password);
                    /* user mnt est une variable de session */
                    req.getSession().setAttribute("user",user);
                    dest = "/WEB-INF/pages/menu.jsp";
                } catch (UtilisateurDejaConnecteException e) {
                    String erreur = "L'utilisateur "+ login +" est déjà connecté !";
                    /*erreur est une variable requete */
                    req.setAttribute("erreur",erreur);
                    dest = "/WEB-INF/pages/menu.jsp";

                }catch (InformationsSaisiesIncoherentesException e){
                    String erreur = "Couple pseudo/password incohérent";
                    req.setAttribute("erreur",erreur);
                    dest = "/WEB-INF/pages/accueil.jsp";
                }
                break;
            case "parier":
                Utilisateur user = (Utilisateur) req.getSession().getAttribute("user");
                String verdict = req.getParameter("verdict");
                Double mise = Double.valueOf(req.getParameter("montant"));
                long idmatch = Long.parseLong(req.getParameter("id"));
                Match m = facade.getMatch(idmatch);
                try {
                    long idPari = facade.parier(user.getLogin(),idmatch,verdict, mise);
                    Pari pari = facade.getPari(idPari);
                    req.setAttribute("pari",pari);
                    dest = "/WEB-INF/pages/msgMatchParie.jsp";
                } catch (MatchClosException e) {
                    String erreur = "Match a déjà fini.";
                    req.setAttribute("erreur",erreur);
                    req.setAttribute("match",m);
                    dest = "/WEB-INF/pages/parieMatch.jsp";
                } catch (ResultatImpossibleException e) {
                    throw new RuntimeException(e);
                } catch (MontantNegatifOuNulException e) {
                    String erreur = "Vous devez saisir un montant positif pour votre mise";
                    req.setAttribute("erreur",erreur);
                    req.setAttribute("match",m);
                    dest = "/WEB-INF/pages/parieMatch.jsp";
                }
                break;
            case "ajoutmatch":
                Utilisateur user1 = (Utilisateur) req.getSession().getAttribute("user");
                String sport = req.getParameter("sport");
                String eq1 = req.getParameter("equipe1");
                String eq2 = req.getParameter("equipe2");
                LocalDateTime date = LocalDateTime.parse(req.getParameter("date"));
                try {
                    long idNvmatch = facade.ajouterMatch(user1.getLogin(),sport,eq1,eq2,date);
                    Match match = facade.getMatch(idNvmatch);
                    req.setAttribute("match",match);
                    dest = "/WEB-INF/pages/mspAjoutNvMatch.jsp";
                } catch (UtilisateurNonAdminException e) {
                    String erreur = e.getMessage();
                    req.setAttribute("erreur",erreur);
                    dest = "/WEB-INF/pages/ajoutMatch.jsp";
                }
                break;
            case "ajoutresultatMatch":
                Utilisateur user2 = (Utilisateur) req.getSession().getAttribute("user");
                String rsmatch = req.getParameter("rsmatch");
                long idmat = Long.parseLong(req.getParameter("id"));
                try {
                    facade.resultatMatch(user2.getLogin(),idmat,rsmatch);
                    Match mat= facade.getMatch(idmat);
                    req.setAttribute("match",mat);
                    dest = "/WEB-INF/pages/msgAjoutResultatMatch.jsp";
                } catch (UtilisateurNonAdminException e) {
                    String erreur = e.getMessage();
                    req.setAttribute("erreur",erreur);
                    dest = "/WEB-INF/pages/menu.jsp";
                } catch (ResultatImpossibleException e) {
                    String erreur = e.getMessage();
                    req.setAttribute("erreur",erreur);
                    dest = "/WEB-INF/pages/ajoutResultatMatch.jsp";
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + cleNav);
        }
        this.getServletContext().getRequestDispatcher(dest).forward(req,resp);
    }
}
