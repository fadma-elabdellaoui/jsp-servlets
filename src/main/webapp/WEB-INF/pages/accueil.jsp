<%--
  Created by IntelliJ IDEA.
  User: Fadma
  Date: 17/01/2024
  Time: 14:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Paris en ligne - Page de connexion</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>

<div class="containerform">
    <jsp:include page="/WEB-INF/pages/common/erreur.jsp"/>
    <h2>Welcome to PEL : </h2>
    <form method="post" action="/home/connexion">
        <div class="form-outline mb-4">
            <label class="form-label" for="feild:login">Pseudo</label>
            <input type="text" id="feild:login" name="login" class="form-control">
        </div>
        <div class="form-outline mb-4">
            <label class="form-label" for="feild:password">Password</label>
            <input type="password" id="feild:password" name="password" class="form-control">
        </div>
        <input type="submit" value="Se connecter" class="btn btn-primary btn-block mb-4">
    </form>
</div>
</body>
</html>
