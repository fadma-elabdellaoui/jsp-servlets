<%--
  Created by IntelliJ IDEA.
  User: Fadma
  Date: 17/01/2024
  Time: 15:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Liste des paris ouverts</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <jsp:useBean id="matchsPasCommences" scope="session" type="java.util.Collection<modele.Match>" />
</head>
<body>
<div class="containerform">
    <jsp:include page="/WEB-INF/pages/common/erreur.jsp"/>
    <ul>
        <c:forEach items="${matchsPasCommences}" var="match">
            <li class="m-3">sport : ${match.sport} - ${match.equipe1} vs ${match.equipe2} -
                le
                <fmt:parseDate value = "${match.quand}" var = "parsedEmpDate" pattern = "yyyy-MM-dd'T'HH:mm" type="both" />
                <fmt:formatDate pattern = "dd-MM-yyyy HH:mm" value = "${parsedEmpDate}" />
                <a href="/home/parier?id=${match.idMatch}" class="btn btn-primary btn-block ">parier</a>
            </li>
        </c:forEach>
    </ul>
    <button type="button" class="btn btn-secondary btn-block mb-4"><a href="/home/menu" class="text-white">Annuler</a></button>
</div>
</body>
</html>
