<%--
  Created by IntelliJ IDEA.
  User: Fadma
  Date: 17/01/2024
  Time: 15:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Confirmation du pari</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <jsp:useBean id="pari" scope="request" type="modele.Pari" />
</head>
<body>
<div class="containerform">
    <div>La mise de ${pari.montant} euros sur le résultat ${pari.vainqueur} pour le match :
    ${pari.match.equipe1} vs ${pari.match.equipe2} le
        <fmt:parseDate value = "${pari.match.quand}" var = "parsedEmpDate" pattern = "yyyy-MM-dd'T'HH:mm" type="both" />
        <fmt:formatDate pattern = "dd-MM-yyyy HH:mm" value = "${parsedEmpDate}" /> a bien été annulée !
    </div>
    <button type="button" class="btn btn-secondary btn-block mb-4"><a href="/home/menu" class="text-white">Retour au menu</a></button>
</div>
</body>
</html>