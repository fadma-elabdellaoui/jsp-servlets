<%--
  Created by IntelliJ IDEA.
  User: Fadma
  Date: 17/01/2024
  Time: 14:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Pari en ligne</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">

</head>
<body>
<div class="containerform">
    <jsp:include page="/WEB-INF/pages/common/welcomeMsg.jsp"/>
    <h2>Menu</h2>
    <ul>
        <jsp:include page="/WEB-INF/pages/admin.jsp"/>
        <li><a href="/home/listeParisOuverts" class="text-black text-decoration-none">Affichier les matchs sur lesquels parier</a></li>
        <li><a href="/home/mesParis" class="text-black text-decoration-none">Afficher mes paris</a></li>
        <li><a href="/home/deconnexion" class="text-black text-decoration-none">Déconnexion</a></li>
    </ul>
</div>
</body>
</html>
