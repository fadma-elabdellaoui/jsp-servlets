<%--
  Created by IntelliJ IDEA.
  User: Fadma
  Date: 25/01/2024
  Time: 16:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.time.LocalDateTime" %>

<html>
<head>
    <title>Ajouter résultat d'un match</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <jsp:useBean id="match" scope="request" type="modele.Match" />
</head>
<body>
    <div class="containerform">
        <jsp:include page="/WEB-INF/pages/common/erreur.jsp"/>
        <h2>Ajouter le résultat du match : </h2>
        <div>Sport : ${match.sport}, ${match.equipe1} vs ${match.equipe2}</div>
        <form method="post" action="/home/ajoutresultatMatch?id=${match.idMatch}">
            <div class="form-outline mb-4">
                <label class="form-label" for="feild:rsmatch">Résultat du match</label>
                <select name="rsmatch" id="feild:rsmatch" class="form-select">
                    <option value="nul">nul</option>
                    <option value="${match.equipe1}">${match.equipe1}</option>
                    <option value="${match.equipe2}">${match.equipe2}</option>
                </select>
            </div>
            <c:choose>
                <c:when test="${!match.quand.isBefore(LocalDateTime.now())}">
                    <div class="alert alert-danger">Vous ne pouvez pas ajouter le résultat d'un match qui n'a pas encore pris fin</div>
                </c:when>
                <c:otherwise>
                    <input type="submit" value="Enregistrer" class="btn btn-primary btn-block mb-4">
                </c:otherwise>
            </c:choose>
            <button type="button" class="btn btn-secondary btn-block mb-4"><a href="/home/menu" class="text-white">Annuler</a></button>
        </form>
    </div>
</body>
</html>
