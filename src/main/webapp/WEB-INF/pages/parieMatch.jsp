<%--
  Created by IntelliJ IDEA.
  User: Fadma
  Date: 17/01/2024
  Time: 15:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Pari sur un match</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <jsp:useBean id="match" scope="request" type="modele.Match" />

</head>
<body>
<div class="containerform">
    <h2>Vous vouler parier sur le match : </h2>
    <h2>${match.equipe1} vs ${match.equipe2}</h2>
    <h2>
        <fmt:parseDate value = "${match.quand}" var = "parsedEmpDate" pattern = "yyyy-MM-dd'T'HH:mm" type="both" />
        <fmt:formatDate pattern = "dd-MM-yyyy HH:mm" value = "${parsedEmpDate}" />
    </h2>
    <jsp:include page="/WEB-INF/pages/common/erreur.jsp"/>

    <form method="post" action="/home/parier?id=${match.idMatch}">
        <div class="form-outline mb-4">
            <label class="form-label" for="feild:verdict">Verdict du match</label>
            <select name="verdict" id="feild:verdict" class="form-select">
                <option value="nul">nul</option>
                <option value="${match.equipe1}">${match.equipe1}</option>
                <option value="${match.equipe2}">${match.equipe2}</option>
            </select>
        </div>
        <div class="form-outline mb-4">
            <label for="feild:montant"> Montant</label>
            <input type="text"
                   id="feild:montant"
                   name="montant"
                   required
                   pattern="[0-9]{1,4}(\.[0-9]{1,2})?"
                   placeholder="Veuillez saisir un montant de type 0000.00"
                   style="width: -webkit-fill-available !important;"
            >
        </div>
        <input type="submit" value="Parier"  class="btn btn-primary btn-block mb-4">
        <button type="button" class="btn btn-secondary btn-block mb-4">
            <a href="/home/menu" class="text-white">Annuler</a>
        </button>
    </form>
</div>
</body>
</html>
