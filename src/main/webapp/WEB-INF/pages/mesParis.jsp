<%--
  Created by IntelliJ IDEA.
  User: Fadma
  Date: 17/01/2024
  Time: 15:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Mes paris</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <jsp:useBean id="mesparis" scope="session" type="java.util.Collection<modele.Pari>" />

</head>
<body>
<div class="containerform">
    <jsp:include page="/WEB-INF/pages/common/erreur.jsp"/>

    <h3>Mes paris :</h3>
    <ul>
        <c:forEach items="${mesparis}" var="pari">
            <li class="m-3">sport : ${pari.match.sport} - ${pari.match.equipe1} vs ${pari.match.equipe2} -
                le
                <fmt:parseDate value = "${pari.match.quand}" var = "parsedEmpDate" pattern = "yyyy-MM-dd'T'HH:mm" type="both" />
                <fmt:formatDate pattern = "dd-MM-yyyy HH:mm" value = "${parsedEmpDate}" />

                Mise de ${pari.montant} sur ${pari.vainqueur}
                <a href="/home/annulerPari?id=${pari.idPari}" class="btn btn-danger btn-block">Annuler</a>
            </li>
        </c:forEach>
    </ul>
    <jsp:include page="/WEB-INF/pages/parisGagne.jsp"/>
    <button type="button" class="btn btn-secondary btn-block mb-4"><a href="/home/menu" class="text-white">Retour au menu</a></button>
</div>
</body>
</html>
