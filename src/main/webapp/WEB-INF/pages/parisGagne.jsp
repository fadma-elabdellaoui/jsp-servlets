<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:useBean id="mesparis" scope="session" type="java.util.Collection<modele.Pari>" />

<h3>les paris gagné : </h3>
<ul>
    <c:forEach items="${mesparis}" var="pari">
        <c:if test="${pari.gain.isPresent() && pari.gain.get() > 0.0}">

            <li>sport : ${pari.match.sport} - ${pari.match.equipe1} vs ${pari.match.equipe2} -
                le
                <fmt:parseDate value = "${pari.match.quand}" var = "parsedEmpDate" pattern = "yyyy-MM-dd'T'HH:mm" type="both" />
                <fmt:formatDate pattern = "dd-MM-yyyy HH:mm" value = "${parsedEmpDate}" />
                Mise de ${pari.montant} sur ${pari.vainqueur}, le gain est : ${pari.gain.orElse(0.0)}
            </li>
        </c:if>
    </c:forEach>
</ul>