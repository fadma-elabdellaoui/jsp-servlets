<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Mes paris</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <jsp:useBean id="matchs" scope="session" type="java.util.Collection<modele.Match>" />

</head>
<body>
<div class="containerform">
    <jsp:include page="/WEB-INF/pages/common/erreur.jsp"/>
    <h3>Les matchs :</h3>
    <ul>
        <c:forEach items="${matchs}" var="match">
            <c:if test="${!match.resultat.isPresent()}">
                <li class="m-3">sport : ${match.sport} - ${match.equipe1} vs ${match.equipe2} -
                    le
                    <fmt:parseDate value = "${match.quand}" var = "parsedEmpDate" pattern = "yyyy-MM-dd'T'HH:mm" type="both" />
                    <fmt:formatDate pattern = "dd-MM-yyyy HH:mm" value = "${parsedEmpDate}" />
                    <a href="/home/ajoutResultatMatch?id=${match.idMatch}" class="btn btn-primary btn-block">Ajouter résultat</a>
                </li>
            </c:if>
        </c:forEach>
    </ul>
    <button type="button" class="btn btn-secondary btn-block mb-4"><a href="/home/menu" class="text-white">Annuler</a></button>
</div>
</body>
</html>
