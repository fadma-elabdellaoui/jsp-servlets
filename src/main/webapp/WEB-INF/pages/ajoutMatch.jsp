<%--
  Created by IntelliJ IDEA.
  User: Fadma
  Date: 24/01/2024
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Créer un match</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <div class="containerform">
        <jsp:include page="/WEB-INF/pages/common/erreur.jsp"/>
        <h2>Créer un nouveau match :</h2>
        <form method="post" action="/home/ajoutmatch">
            <div class="form-outline mb-4">
                <label class="form-label" for="feild:sport">Sport :</label>
                <input class="form-control" type="text" id="feild:sport" name="sport" required>
            </div>
            <div class="form-outline mb-4">
                <label class="form-label" for="feild:equipe1">Equipe 1 :</label>
                <input class="form-control" type="text" id="feild:equipe1" name="equipe1" required>
            </div>
            <div class="form-outline mb-4">
                <label class="form-label" for="feild:equipe2">Equipe 2 :</label>
                <input class="form-control" type="text" id="feild:equipe2" name="equipe2" required>
            </div>
            <div class="form-outline mb-4">
                <label class="form-label" for="feild:date">Date du match :</label>
                <input class="form-control" type="datetime-local" id="feild:date" name="date" required>
            </div>
            <input type="submit" value="Ajouter" class="btn btn-primary btn-block mb-4">
            <button type="button" class="btn btn-secondary btn-block mb-4"><a href="/home/menu" class="text-white">Annuler</a></button>
        </form>
    </div>
</body>
</html>
