<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Fadma
  Date: 25/01/2024
  Time: 16:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Confirmation ajout résultat Match</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <jsp:useBean id="match" scope="request" type="modele.Match" />
</head>
<body>
<div class="containerform">

<div>vous avez ajouté le résultat du match :
    Sport : ${match.sport}, ${match.equipe1} vs ${match.equipe2} le
    <fmt:parseDate value = "${match.quand}" var = "parsedEmpDate" pattern = "yyyy-MM-dd'T'HH:mm" type="both" />
    <fmt:formatDate pattern = "dd-MM-yyyy HH:mm" value = "${parsedEmpDate}" />
    Résultat : ${match.resultat.get()}.
</div>
    <button type="button" class="btn btn-secondary btn-block mb-4"><a href="/home/menu" class="text-white">Retour au menu</a></button>
</div>

</body>
</html>
