<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="user" scope="session" type="modele.Utilisateur" />

<c:if test="${user.admin}">
    <li><a href="/home/ajoutMatch" class="text-black text-decoration-none">Ajouter un match</a></li>
    <li><a href="/home/matchs" class="text-black text-decoration-none">Ajouter résultat d'un match</a></li>
</c:if>
