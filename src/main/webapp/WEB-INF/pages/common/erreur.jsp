<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="erreur" class="java.lang.String" scope="request" type="java.lang.String"/>

<c:if test="${!empty(erreur)}">
    <div class="alert alert-danger">${erreur}</div>
</c:if>
