<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Fadma
  Date: 24/01/2024
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Confirmation d'ajout d'un NV match</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <jsp:useBean id="match" scope="request" type="modele.Match" />
</head>
<body>
<div class="containerform">
    <div>Vous avez ajouté un nouveau match : </div>
    <li>Sport : ${match.sport}</li>
    <li>Equipe 1 : ${match.equipe1}</li>
    <li>Equipe 2 : ${match.equipe2}</li>
    <li>Date :
        <fmt:parseDate value = "${match.quand}" var = "parsedEmpDate" pattern = "yyyy-MM-dd'T'HH:mm" type="both" />
        <fmt:formatDate pattern = "dd-MM-yyyy HH:mm" value = "${parsedEmpDate}" /></li>
    <button type="button" class="btn btn-secondary btn-block mb-4"><a href="/home/menu" class="text-white">Retour au menu</a></button>
</div>
</body>
</html>
