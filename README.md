### PEL : Paris En Ligne
***

> _Mini-projet_ : Site web de paris en ligne sur les résultats de matchs sportifs.

> Ce site est développé en utilisant les JSPs statiques et le contrôleur (servlet) permettant de gérer cette navigation

Le fonctionnement général du site web est le suivant :

- Un administrateur du site ajoute un match, avec le sport (foot, rugby,…), les noms des deux équipes qui doivent se rencontrer, la date et l’heure du match.
- Les joueurs peuvent alors parier sur le résultat final du match, en prédisant le vainqueur (ou match nul) et en misant une somme d’argent en euros.
- Les joueurs peuvent continuer à parier jusqu’à la dernière seconde avant le début du match, ensuite les paris sont clos sur ce match.
- Les joueurs peuvent aussi annuler leurs propres paris (ou un admin peut le faire) jusqu’au début du match.

> _Exemple_ : Le 16 juin 2024, à 12h00, match de foot France-Australie, pari de « UtilisateurX » sur un résultat match nul, 8€.

- Une fois le match terminé, un administrateur saisit le résultat (équipe vainqueur ou nul) de ce match.

- Les gains sont alors répartis automatiquement entre les parieurs qui ont gagné (l’ensemble des mises sur le match moins 10% récupérés par le site sont répartis entre les gagnants en proportion de leurs mises).

- Les utilisateurs, qui s’identifient en tapant leur login et mot de passe,
  - Peuvent voir la liste des matchs encore ouverts aux paris.
  - Peuvent parier sur un match ouvert et gérer ses paris.
  
- Partie _Administration_ : 
  - L'administrateur a les mêmes options qu'un simple utilisateur
  - Il peut ajouter de nouveaux matches.
  - Il peut ajouter les résultats des matches.

  
## Modélisation de l'application
***

![Modélisation](modélisation.png)


### Les comptes :
***
- compte joueur :
  - pseudo : user1
  - password : secret
- compte Admin  : 
  - pseudo : admin
  - password : secret
  
### Lance l'app :
- Run de l’application en ligne de commande : 
> mvn jetty:run
> 
> Ouvrir un navigateur
> 
> Tapez une URL gérée par le servlet : localhost:8080/home/
